module.exports = (env, argv) => {
    const path = require("path")
    const WorkboxWebpackPlugin = require("workbox-webpack-plugin")

    return {
        mode: "production",
        plugins: [
            new WorkboxWebpackPlugin.InjectManifest({
                swSrc: path.resolve(__dirname, "src", "service-worker.ts")
            }),
        ]
    }
}