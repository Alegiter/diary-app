import { signal } from "@preact/signals"
import { createContext } from "preact"
import dayjs from "dayjs"
import { AppEntry } from "@app/model/AppEntry"

export function createAppState() {
    const calendar = <const>{
        date: signal(dayjs().startOf("D").toDate()),
        entries: signal<Array<AppEntry>>([]),
    }
    return <const>{
        calendar
    }
}

export const AppState = createContext(createAppState())
