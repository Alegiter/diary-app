import { Signal } from "@preact/signals"
import { ConfigContext as AntdInnerConfigContext } from "antd/es/config-provider"
import useAntdInnerInputStyle from "antd/es/input/style"
import { CSSProperties } from "preact/compat"
import { useCallback, useRef, useContext } from "preact/hooks"

type Props = {
    text: Signal<string>
    onChange: (value: string) => void
}

export function Wysiwyg(props: Props) {
    const { text, onChange } = props
    const contentEditableRef = useRef<HTMLDivElement>(null)
    const hashId = useAntdInnerInputHashId()

    const onInput = useCallback(() => {
        const contentEditable = contentEditableRef.current
        if (!contentEditable) {
            return
        }

        onChange(contentEditable.innerHTML)
    }, [])

    return (
        <div
            ref={contentEditableRef}
            contentEditable={true}
            style={style}
            className={["ant-input", hashId].join(" ")}
            onInput={onInput}
            dangerouslySetInnerHTML={{ __html: text.value }}
        />
    )
}

function useAntdInnerInputHashId(): string {
    const { getPrefixCls } = useContext(AntdInnerConfigContext)
    const prefixCls = getPrefixCls('input')
    const [, hashId] = useAntdInnerInputStyle(prefixCls)
    return hashId
}

const style: CSSProperties = {
    borderRadius: 0,
    flex: 1,
    overflow: "overlay",
}