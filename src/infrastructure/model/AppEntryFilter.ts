export type AppEntryFilter = {
    date?: Date
    period?: {
        from: Date
        to: Date
    }
    firstMatch?: boolean
}