import { AppEntry, AppEntryDraft } from "@app/model/AppEntry"
import { Service } from "typedi"
import { AppEntryFilter } from "@app/model/AppEntryFilter"
import { AppJson } from "@app/utils/json"

export const EntryTransport$Token = "EntryTransport"
export interface EntryTransport {
    getEntry(id: number): Promise<AppEntry | undefined>
    putEntry(value: AppEntry | AppEntryDraft): Promise<number>
    search(filter: AppEntryFilter): Promise<Array<AppEntry>>
}

@Service(EntryTransport$Token)
export class EntryTransportImpl implements EntryTransport {
    async getEntry(id: number): Promise<AppEntry | undefined> {
        const {parse} = AppJson
        const response = await fetch(`/api/v1/entry/${id}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
        if (response.status === 200) {
            const entry = parse<AppEntry>(await response.text())
            return entry
        }
        return undefined
    }

    async putEntry(value: AppEntry | AppEntryDraft): Promise<number> {
        const {stringify, parse} = AppJson
        const response = await fetch(`/api/v1/entry`, {
            method: "PUT",
            body: stringify(value),
            headers: {
                "Content-Type": "application/json"
            }
        })
        const idHolder = parse<{ id: number }>(await response.text())
        return idHolder.id
    }
    async search(filter: AppEntryFilter): Promise<AppEntry[]> {
        const {stringify, parse} = AppJson
        const response = await fetch(`/api/v1/search`, {
            method: "POST",
            body: stringify(filter),
            headers: {
                "Content-Type": "application/json"
            }
        })
        const entries = parse<Array<AppEntry>>(await response.text())
        return entries
    }
}
