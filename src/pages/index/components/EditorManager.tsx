import { useCallback, useContext, useEffect } from "preact/hooks"
import { Editor } from "./Editor"
import { AppState } from "@app/view/state/context"
import { useIocContainer } from "@app/view/hook/useIocContainer"
import { EntryService$Token } from "@app/service/EntryService"
import { useSignal, Signal } from "@preact/signals"

export function EditorManager() {
    const { text, onSave } = useEntrySignal()

    return (
        <Editor
            text={text}
            onSave={onSave}
        />
    )
}

function useEntrySignal(): {
    text: Signal<string>
    onSave: () => Promise<void>
} {
    const { date } = useContext(AppState).calendar
    const entryService = useIocContainer(EntryService$Token)

    const text = useSignal("")
    const id = useSignal<number | undefined>(undefined)

    useEffect(() => {
        entryService.getByDate(date.value)
            .then((entry) => {
                text.value = entry.text
                id.value = entry.id
            })
            .catch(() => {
                text.value = ""
                id.value = undefined
            })
    }, [date.value])

    const onSave = useCallback(async () => {
        const newId = await entryService.put({
            date: date.value,
            text: text.value,
            ...(id.value && { id: id.value })
        })
        id.value = newId
    }, [])

    return {
        text,
        onSave
    }
}