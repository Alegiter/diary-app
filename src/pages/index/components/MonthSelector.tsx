import { Select, SelectProps } from "antd"
import { Dayjs } from "dayjs"
import { useCallback, useMemo } from "preact/hooks"

type Props<DateType> = {
    value: DateType
    onChange: (date: DateType) => void
} & Omit<SelectProps, "value" | "onChange">

export function MonthSelector(props: Props<Dayjs>) {
    const { value: datejs, onChange, ...rest } = props
    const month = datejs.month()
    const months = useMonths(datejs)

    const onSelect = useCallback((newMonth: number) => {
        const now = datejs.clone().month(newMonth)
        onChange(now)
    }, [datejs])

    return (
        <Select
            {...rest}
            value={month}
            onChange={onSelect}
        >
            {months.map((month, i) => (
                <Select.Option
                    key={month}
                    value={i}
                >
                    {month}
                </Select.Option>
            ))}
        </Select>
    )
}

function useMonths(datejs: Dayjs) {
    const months = useMemo(() => {
        let current = datejs.clone()
        const localeData = datejs.localeData()
        const result = []
        for (let i = 0; i < 12; i++) {
            current = current.month(i)
            result.push(localeData.monthsShort(current))
        }
        return result
    }, [datejs.month()])
    return months
}