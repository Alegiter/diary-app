import { Token, Container } from "typedi"

export function useIocContainer<T>(token: Token<T>): T {
    return Container.get(token)
}