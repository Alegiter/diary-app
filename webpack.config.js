module.exports = (env, argv) => {
  const isProduction = env.production

  console.log("-----Build-----")
  console.log(`Mode: ${isProduction ? "production" : "development"}`);
  console.log("---------------");
  
  const commonConfig = require("./webpack.part.common")
  const devConfig = require("./webpack.part.dev")
  const prodConfig = require("./webpack.part.prod")
  const { merge } = require("webpack-merge")

  const merged =  merge(
    commonConfig(env, argv),
    isProduction ? prodConfig(env, argv) : devConfig(env, argv)
  )
  
  console.log(JSON.stringify(merged, null, 2))

  return merged
}
