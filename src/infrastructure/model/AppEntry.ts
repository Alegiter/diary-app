export type AppEntry = {
    id: number
    date: Date
    text: string
}

export type AppEntryDraft = Omit<AppEntry, "id">