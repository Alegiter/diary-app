import { AppEntry, AppEntryDraft } from "@app/model/AppEntry"
import { EntryTransport, EntryTransport$Token } from "@app/transport/EntryTransport"
import { Token, Service, Inject } from "typedi"

export const EntryService$Token = new Token<EntryService>("EntryService")

export interface EntryService {
    put(entry: AppEntry | AppEntryDraft): Promise<number>
    getByDate(date: Date): Promise<AppEntry>
}

@Service(EntryService$Token)
export class EntryServiceImpl implements EntryService {
    async put(entry: AppEntry | AppEntryDraft): Promise<number> {
        return this.entryTransport.putEntry(entry)
    }

    async getByDate(date: Date): Promise<AppEntry> {
        console.log("EntryService.getByDate", date)

        const [result,] = await this.entryTransport.search({
            date,
            firstMatch: true
        })
        if (result === undefined) {
            throw "Not found"
        }
        console.log("EntryService.getByDate | result", result)
        return result
    }

    constructor(
        @Inject(EntryTransport$Token) private entryTransport: EntryTransport
    ) { }
}
