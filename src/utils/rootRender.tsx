import { render, FunctionComponent } from "preact"

export function rootRender(App: FunctionComponent): void {
    const root = document.getElementById("root")!
    render(<App />, root)
}