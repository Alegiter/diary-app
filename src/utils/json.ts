export const AppJson = makeJson([
    jsonPluginDate({
        equal: ["date", "from", "to"]
    })
])

type Json = {
    parse: <T>(text: string) => T
    stringify: (value: unknown) => string
}

type JsonPlugin = {
    parse: (key: string, value: any) => any
    stringify: (key: string, value: any) => string
}

export function makeJson(plugins: Array<JsonPlugin> = []): Json {
    const parse = (text: string) => {
        return JSON.parse(text, (key: string, value: any) => {
            plugins.forEach(plugin => {
                value = plugin.parse(key, value)
            })
            return value
        })
    }
    const stringify = (value: unknown) => {
        return JSON.stringify(value, (key: string, value: any) => {
            plugins.forEach(plugin => {
                value = plugin.stringify(key, value)
            })
            return value
        })
    }
    return {
        parse,
        stringify,
    }
}

export function jsonPluginDate(match: {
    equal: Array<string>
}): JsonPlugin {
    const { equal } = match
    return {
        parse: (key: string, value: any) => {
            if (equal.includes(key) && (typeof value === "number" || typeof value === "string")) {
                return new Date(value)
            }
            return value
        },
        stringify: (key: string, value: any) => {
            if (value instanceof Date) {
                return value.getTime()
            }
            return value
        }
    }
}