import { AppEntry, AppEntryDraft } from "@app/model/AppEntry"
import { Inject, Service, Token } from "typedi"
import { AppDataBase, AppDataBase$Token } from "../db/AppDataBase"
import { AppJson } from "@app/utils/json"

export const EntryController$Token = new Token<EntryController>("EntryController")

export interface EntryController {
    get(req: Request): Promise<Response>
    put(req: Request): Promise<Response>
}

@Service(EntryController$Token)
export class EntryControllerImpl implements EntryController {
    async get(req: Request): Promise<Response> {
        const { stringify } = AppJson
        const id = Number(req.url.substring(req.url.lastIndexOf("/") + 1))
        const entry = await this.db.get(id)
        if (entry === undefined) {
            return new Response(null, { status: 404 })
        }
        return new Response(stringify(entry), { status: 200 })
    }
    async put(req: Request): Promise<Response> {
        const { parse, stringify } = AppJson
        const entry = parse<AppEntry | AppEntryDraft>(await req.text())
        if ("id" in entry) {
            await this.db.set(entry.id, entry)
            return new Response(stringify({
                id: entry.id
            }), { status: 200 })
        }
        const newId = await this.db.add(entry)
        return new Response(stringify({
            id: newId
        }), { status: 200 })
    }
    constructor(
        @Inject(AppDataBase$Token) private db: AppDataBase
    ) { }
}