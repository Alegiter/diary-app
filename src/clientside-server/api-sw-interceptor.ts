/// <reference lib="WebWorker" />

import "reflect-metadata"
import { createApi } from "./api/api"

declare const self: ServiceWorkerGlobalScope

export function registerInterceptor() {
    const api = createApi()
    console.log("sw | api | registerInterceptor", api)
    self.addEventListener("fetch", (event) => {
        console.debug("sw | api | fetch", event)
        
        const req = event.request
        
        for (const [urlRegexp, fn] of api) {
            if (urlRegexp.test(req.url)) {
                event.respondWith(fn(req))
                return
            }
        }
    })
}
