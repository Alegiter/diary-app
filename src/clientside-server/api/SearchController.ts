import { Inject, Service, Token } from "typedi"
import { AppDataBase, AppDataBase$Token } from "../db/AppDataBase"
import { AppEntryFilter } from "@app/model/AppEntryFilter"
import { AppJson } from "@app/utils/json"

export const SearchController$Token = new Token<SearchController>("SearchController")

export interface SearchController {
    post(req: Request): Promise<Response>
}

@Service(SearchController$Token)
export class SearchControllerImpl implements SearchController {
    async post(req: Request): Promise<Response> {
        const { parse, stringify } = AppJson
        const filter = parse<AppEntryFilter>(await req.text())
        const entries = await this.db.search(filter)
        return new Response(stringify(entries), { status: 200 })
    }

    constructor(
        @Inject(AppDataBase$Token) private db: AppDataBase
    ) { }
}