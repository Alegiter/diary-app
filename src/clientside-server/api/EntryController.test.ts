import { Container } from "typedi"
import { AppEntryDraft } from "@app/model/AppEntry"
import dayjs from "dayjs"
import { EntryController, EntryController$Token } from "./EntryController"
import { AppJson } from "@app/utils/json"

describe("EntryController test", () => {
    let controller: EntryController

    beforeAll(() => {
        Container.set(IDBFactory, self.indexedDB)
        controller = Container.get(EntryController$Token)
    })

    it("should entry put", async () => {
        const { stringify, parse } = AppJson
        const entry: AppEntryDraft = {
            date: dayjs("2023/8/25").startOf("D").toDate(),
            text: "test"
        }
        const reqPut = new Request("", {
            method: "PUT",
            body: stringify(entry)
        })
        const id = await controller.put(reqPut)
            .then(response => response.text())
            .then(text => parse<{ id: number }>(text))
            .then(({ id }) => id)

        const reqGet = new Request(`/${id}`)
        const result = await controller.get(reqGet)
            .then(response => response.text())
            .then(text => parse<AppEntryDraft>(text))

        expect(result).toEqual(entry)
    })
})