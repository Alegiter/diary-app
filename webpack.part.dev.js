module.exports = (env, argv) => {
    const path = require("path")

    return {
        mode: "development",
        devtool: 'eval-source-map',
        devServer: {
            host: "localhost",
            port: 3000,
            historyApiFallback: {
                index: "/404.html"
            }
        },
        entry: {
            serviceWorker: {
                import: path.resolve(__dirname, "src", "service-worker.ts"),
                filename: "service-worker.js"
            }
        },
        optimization: {
            splitChunks: {
                chunks(chunk) {
                    return chunk.name !== "serviceWorker"
                }
            }
        }
    }
}