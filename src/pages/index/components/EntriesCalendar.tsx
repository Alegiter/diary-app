import { Calendar, Col, ConfigProvider, Row, CalendarProps as _CalendarProps, ThemeConfig } from "antd"
import { useCallback, useContext, useEffect, useMemo } from "preact/hooks"
import dayjs, { Dayjs } from "dayjs"
import "dayjs/plugin/localeData"
import { AppState } from "@app/view/state/context"
import { useIocContainer } from "@app/view/hook/useIocContainer"
import { SearchService$Token } from "@app/service/SearchService"
import { theme } from "antd"
import { AppEntry } from "@app/model/AppEntry"
import { MonthSelector } from "./MonthSelector"
import { YearSelector } from "./YearSelector"

const calendarTheme: ThemeConfig = {
    components: {
        Calendar: {
            borderRadius: 0,
            algorithm: true
        }
    }
}

export function EntriesCalendar() {
    const { calendar } = useContext(AppState)
    const service = useIocContainer(SearchService$Token)
    const value = dayjs(calendar.date.value)

    const onDateChange: OnChange = useCallback((datejs) => {
        if (datejs.isSame(value, "M") === false) {
            void onUpdateCells(datejs)
        }

        const date = datejs.startOf("D").toDate()
        calendar.date.value = date
    }, [value.month()])

    const onUpdateCells = useCallback(async (datejs: Dayjs) => {
        const result = await service.search({
            period: {
                from: datejs.startOf("M").toDate(),
                to: datejs.endOf("M").toDate(),
            },
        })
        calendar.entries.value = result
    }, [])

    useEffect(() => {
        void onUpdateCells(dayjs(calendar.date.peek()))
    }, [])

    return (
        <ConfigProvider theme={calendarTheme}>
            <Calendar
                fullscreen={false}
                onChange={onDateChange}
                cellRender={CellContainsEntry}
                mode="month"
                headerRender={Header}
                value={value}
            />
        </ConfigProvider>
    )
}

const { useToken } = theme

const Header: HeaderRender = ({ value: datejs, onChange }) => {
    return (
        <div style={{ padding: 8 }}>
            <Row gutter={8}>
                <Col>
                    <YearSelector
                        size="small"
                        popupMatchSelectWidth={false}
                        value={datejs}
                        onChange={onChange}
                    />
                </Col>
                <Col>
                    <MonthSelector
                        size="small"
                        popupMatchSelectWidth={false}
                        value={datejs}
                        onChange={onChange}
                    />
                </Col>
            </Row>
        </div >
    )
}

const CellContainsEntry: CellRender = (datejs, info) => {
    const { entries } = useContext(AppState).calendar
    const { token } = useToken()
    const match = useMemo(() => {
        return entries.value.findIndex(findSameDate(datejs, info.type)) !== -1
    }, [entries.value, datejs.toDate().getTime(), info.type])
    const color = match ? token.colorPrimary : "transparent"
    return <div style={{ height: "1px", backgroundColor: color }} />
}

function findSameDate(datejs: Dayjs, type: "month" | "date" | string) {
    return (entry: AppEntry): boolean => {
        switch (type) {
            case "date":
            case "month":
                return dayjs(entry.date).isSame(datejs, type)
            default:
                return false
        }
    }
}

type CalendarProps = Required<_CalendarProps<Dayjs>>
type OnChange = CalendarProps["onChange"]
type CellRender = CalendarProps["cellRender"]
type HeaderRender = CalendarProps["headerRender"]
