import { EntryController$Token } from "./EntryController"
import { SearchController$Token } from "./SearchController"
import { Container } from "typedi"

type Handler = (req: Request) => Promise<Response>

export function createApi() {
    Container.set(IDBFactory, self.indexedDB)
    const entry = Container.get(EntryController$Token)
    const search = Container.get(SearchController$Token)
    const origin = self.location.origin
    return new Map<RegExp, Handler>([
        [new RegExp(`${origin}/api/v1/entry/\\d+$`), entry.get.bind(entry)],
        [new RegExp(`${origin}/api/v1/entry$`), entry.put.bind(entry)],
        [new RegExp(`${origin}/api/v1/search$`), search.post.bind(entry)],
    ])
}
