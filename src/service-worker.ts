/// <reference lib="WebWorker" />
import { clientsClaim } from 'workbox-core'
import { precacheAndRoute } from 'workbox-precaching'
import { registerInterceptor } from "./clientside-server/api-sw-interceptor"

declare const self: ServiceWorkerGlobalScope

console.log("sw | clientsClaim")
clientsClaim()

//@ts-ignore
const manifest = self.__WB_MANIFEST
if (manifest && __production__) {
    precacheAndRoute(manifest)
}

self.addEventListener('install', event => {
    console.log('sw |Service Worker installed')
})

self.addEventListener('activate', event => {
    console.log('sw |Service Worker activated')
})

registerInterceptor()

self.skipWaiting()