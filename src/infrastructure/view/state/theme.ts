import { theme, ThemeConfig } from "antd"

const { darkAlgorithm } = theme

export function createAppTheme(): ThemeConfig {
    return {
        algorithm: darkAlgorithm
    }
}