import { AppEntryDraft } from "@app/model/AppEntry"
import { AppDataBase, AppDataBase$Token } from "./AppDataBase"
import { Container } from "typedi"

describe("AppDataBase test", () => {
    let AppDataBase: AppDataBase

    beforeAll(() => {
        Container.set(IDBFactory, self.indexedDB)
        AppDataBase = Container.get(AppDataBase$Token)
    })

    beforeEach(async () => {
        await AppDataBase.clear()
    })

    it("should add/get", async () => {
        const expected: AppEntryDraft = {
            date: new Date(),
            text: "text"
        }
        const id = await AppDataBase.add(expected)
        const actual = await AppDataBase.get(id)
        expect(actual?.text).toBe(expected.text)
    })

    it("should add/set/get", async () => {
        const expected: AppEntryDraft = {
            date: new Date(),
            text: "text"
        }
        const expectedText = "text2"
        const id = await AppDataBase.add(expected)
        await AppDataBase.set(id, {
            ...expected,
            text: expectedText
        })
        const actual = await AppDataBase.get(id)
        expect(actual?.text).toBe(expectedText)
    })

    it("should clear", async () => {
        const expected: AppEntryDraft = {
            date: new Date(),
            text: "text"
        }
        const id = await AppDataBase.add(expected)
        const actual = await AppDataBase.get(id)
        expect(actual).toStrictEqual(expected)
        await AppDataBase.clear()
        const actual2 = await AppDataBase.get(id)
        expect(actual2).toBe(undefined)
    })

    it("should search | date", async () => {
        const date = new Date("2023/8/21")
        await AppDataBase.add({
            date,
            text: "text"
        })
        await AppDataBase.add({
            date,
            text: "text1"
        })
        const result = await AppDataBase.search({
            date,
        })
        expect(result.length).toBe(2)
    })

    it("should search | firstMatch", async () => {
        const date = new Date("2023/8/21")
        await AppDataBase.add({
            date,
            text: "text"
        })
        await AppDataBase.add({
            date,
            text: "text1"
        })
        const result = await AppDataBase.search({
            date,
            firstMatch: true
        })
        expect(result.length).toBe(1)
        expect(result[0].text).toBe("text")
    })
})