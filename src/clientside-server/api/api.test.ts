import { Container } from "typedi"
import { createApi } from "./api"

describe("Api test", () => {
    let api: ReturnType<typeof createApi>
    beforeAll(() => {
        Container.set(IDBFactory, window.indexedDB)
        api = createApi()
    })

    it("should test entry get", () => {
        const url = `/api/v1/entry/${1}`
        for (const [urlRegexp,] of api) {
            if (urlRegexp.test(url)) {
                expect(1).toBe(1)
            }
        }
    })
})