import { Signal } from "@preact/signals"
import { Button } from "antd"
import { useCallback, useState } from "preact/hooks"
import { Wysiwyg } from "./Wysiwyg"
import { CSSProperties } from "preact/compat"

type Props = {
    text: Signal<string>
    onSave: () => Promise<void>
}

export function Editor(props: Props) {
    const { text, onSave } = props
    const [loading, setLoading] = useState(false)
    
    const onChange = useCallback((value: string) => {
        text.value = value
    }, [])
    
    const onSaveEntry = useCallback(async () => {
        setLoading(true)
        await onSave()
        setLoading(false)
    }, [onSave])

    return (
        <div style={style}>
            <Wysiwyg text={text} onChange={onChange} />
            <br/>
            <div>
                <Button type="primary" disabled={loading} onClick={onSaveEntry}>
                    Save
                </Button>
            </div>
        </div>
    )
}

const style: CSSProperties = {
    display: "flex",
    flexDirection: "column",
    height: "100%"
}