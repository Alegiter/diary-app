import { AppEntry, AppEntryDraft } from "@app/model/AppEntry"
import { AppEntryFilter } from "@app/model/AppEntryFilter"
import { Service, Token } from "typedi"

export const AppDataBase$Token = new Token<AppDataBase>("AppDataBase")

export interface AppDataBase {
    get(id: number): Promise<AppEntry | undefined>
    set(id: number, value: AppEntryDraft): Promise<void>
    add(value: AppEntryDraft): Promise<number>
    clear(): Promise<void>
    search(filter: AppEntryFilter): Promise<Array<AppEntry>>
}

@Service(AppDataBase$Token)
export class AppDataBaseImpl implements AppDataBase {
    async get(id: number): Promise<AppEntry | undefined> {
        const db = await this.open()
        return new Promise((resolve, reject) => {
            const transition = db.transaction(this.storeName)
            transition.oncomplete = () => {
                db.close()
            }
            transition.onerror = () => {
                console.error("AppDataBase.set | error", transition.error)
                db.close()
                reject(transition.error)
            }
            const store = transition.objectStore(this.storeName)
            const query = store.get(id)
            query.onsuccess = () => {
                resolve(query.result)
            }
        })
    }

    async set(id: number, value: AppEntry): Promise<void> {
        const db = await this.open()
        return new Promise((resolve, reject) => {
            const transition = db.transaction(this.storeName, "readwrite")
            transition.oncomplete = () => {
                db.close()
            }
            transition.onerror = () => {
                console.error("AppDataBase.set | error", transition.error)
                db.close()
                reject(transition.error)
            }
            const store = transition.objectStore(this.storeName)
            const query = store.put(value, id)
            query.onsuccess = () => {
                resolve()
            }
        })
    }

    async add(value: AppEntryDraft): Promise<number> {
        const db = await this.open()
        return new Promise((resolve, reject) => {
            const transition = db.transaction(this.storeName, "readwrite")
            transition.oncomplete = () => {
                db.close()
            }
            transition.onerror = () => {
                console.error("AppDataBase.add | error", transition.error)
                db.close()
                reject(transition.error)
            }
            const store = transition.objectStore(this.storeName)
            const query = store.add(value)
            query.onsuccess = () => {
                // Because "autoIncrement"
                const id = query.result as number
                resolve(id)
            }
        })
    }

    async clear(): Promise<void> {
        const db = await this.open()
        return new Promise((resolve, reject) => {
            const transition = db.transaction(this.storeName, "readwrite")
            transition.oncomplete = () => {
                db.close()
            }
            transition.onerror = () => {
                console.error("AppDataBase.clear | error", transition.error)
                db.close()
                reject(transition.error)
            }
            const store = transition.objectStore(this.storeName)
            const query = store.clear()
            query.onsuccess = () => {
                resolve()
            }
        })
    }

    async search(filter: AppEntryFilter): Promise<Array<AppEntry>> {
        console.log("AppDataBase.search", filter)

        const db = await this.open()
        return new Promise((resolve, reject) => {
            const result: Array<AppEntry> = []

            const transition = db.transaction(this.storeName)
            transition.oncomplete = () => {
                resolve(result)
                db.close()
            }
            transition.onerror = () => {
                console.error("AppDataBase.search | error", transition.error)
                db.close()
                reject(transition.error)
            }

            const store = transition.objectStore(this.storeName)
            const query = store.openCursor()

            query.onsuccess = () => {
                const cursor = query.result
                if (cursor) {
                    const entry = cursor.value as Omit<AppEntry, "id">
                    if (
                        (filter.date !== undefined && entry.date.getTime() === filter.date.getTime()) ||
                        (filter.period !== undefined && entry.date.getTime() > filter.period.from.getTime() && entry.date.getTime() < filter.period.to.getTime())
                    ) {
                        result.push({
                            ...entry,
                            id: cursor.key as number,
                        })
                    }
                    
                    if (filter.firstMatch && result.length === 1) {
                        return
                    }
                    cursor.continue()
                } else {
                    console.log("AppDataBase.search | result", result)
                }
            }
        })
    }

    private open(): Promise<IDBDatabase> {
        return new Promise((resolve, reject) => {
            const request = this.indexedDB.open(this.dbName, this.dbVersion)
            request.onsuccess = () => {
                resolve(request.result)
            }
            request.onerror = () => {
                reject(request.error)
            }
            request.onupgradeneeded = () => {
                const db = request.result
                db.createObjectStore(this.storeName, { autoIncrement: true })
            }
        })
    }

    constructor(private indexedDB: IDBFactory) { }

    private dbName = "DiaryApp"
    private dbVersion = 1
    private storeName = "entriesStore"
}
