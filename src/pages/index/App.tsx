import { ConfigProvider, Row, Col, Layout } from "antd"
import { EntriesCalendar } from "./components/EntriesCalendar"
import { EditorManager } from "./components/EditorManager"
import { createAppTheme } from "@app/view/state/theme"
import { CSSProperties } from "preact/compat"


export function App() {
    return (
        <ConfigProvider theme={createAppTheme()}>
            <Layout>
                <Layout.Content style={contentStyle}>
                    <Row gutter={16}>
                        <Col flex="300px">
                            {/* <div>Search</div> */}
                            <div>
                                <EntriesCalendar />
                            </div>
                        </Col>
                        <Col flex="auto" style={rightStyle}>
                            <EditorManager />
                        </Col>
                    </Row>
                </Layout.Content>
            </Layout>
        </ConfigProvider>
    )
}

const contentStyle: CSSProperties = { padding: "8px" }
const rightStyle: CSSProperties = { height: "calc(100svh - 16px)" }