import { AppEntry } from "@app/model/AppEntry"
import { AppEntryFilter } from "@app/model/AppEntryFilter"
import { EntryTransport, EntryTransport$Token } from "@app/transport/EntryTransport"
import { Token, Service, Inject } from "typedi"

export const SearchService$Token = new Token<SearchService>("SearchService")

export interface SearchService {
    search(filter: AppEntryFilter): Promise<Array<AppEntry>>
}

@Service(SearchService$Token)
export class SearchServiceImpl implements SearchService {

    async search(filter: AppEntryFilter): Promise<AppEntry[]> {
        return this.entryTransport.search(filter)
    }

    constructor(
        @Inject(EntryTransport$Token) private entryTransport: EntryTransport
    ) { }
}
