const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')
const webpack = require("webpack")

const dir = __dirname

module.exports = (env, argv) => {
    const isProduction = env.production
    const pages = makePages(isProduction)

    return {
        bail: isProduction,
        context: path.resolve(dir, "src", "pages"),
        entry: pages.entry,
        output: {
            clean: true,
            path: path.resolve(dir, "dist"),
            filename: "[name].[contenthash].js",
        },
        plugins: [
            isProduction && new MiniCssExtractPlugin(),
            new webpack.DefinePlugin({
                __production__: JSON.stringify(isProduction)
            }),
        ]
            .concat(pages.plugins)
            .filter(Boolean),
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/i,
                    loader: "ts-loader",
                    exclude: ["/node_modules/"],
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        isProduction ? MiniCssExtractPlugin.loader : "style-loader",
                        "style-loader",
                        "css-loader",
                        "sass-loader"
                    ],
                },
                {
                    test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
                    type: "asset",
                },
            ],
        },
        resolve: {
            extensions: [".tsx", ".ts", ".jsx", ".js", "..."],
            plugins: [
                new TsconfigPathsPlugin()
            ],
        },
        optimization: {
            splitChunks: {
                chunks: "all"
            }
        }
    }
}

function makePages(isProduction) {
    const pages = ["index", "search", "404"]
    
    return {
        entry: pages.reduce((config, page) => {
            config[page] = `./${page}/index.ts`
            return config
        }, {}),
        plugins: pages.map((page) => {
            return new HtmlWebpackPlugin({
                template: path.resolve(dir, "index.html"),
                filename: `${page}.html`,
                chunks: [page],
                minify: isProduction,
                publicPath: "/"
            })
        })
    }
}