import { Select, SelectProps } from "antd"
import { Dayjs } from "dayjs"
import { useCallback, useMemo } from "preact/hooks"

type Props<DateType> = {
    value: DateType
    onChange: (date: DateType) => void
    offset?: number
} & Omit<SelectProps, "value" | "onChange">

export function YearSelector(props: Props<Dayjs>) {
    const { value: datejs, onChange, offset = 10, ...rest } = props
    const month = datejs.year()
    const years = useYears(datejs, offset)

    const onSelect = useCallback((newMonth: number) => {
        const now = datejs.clone().year(newMonth)
        onChange(now)
    }, [datejs])

    return (
        <Select
            {...rest}
            value={month}
            onChange={onSelect}
        >
            {years.map((year) => (
                <Select.Option
                    key={year}
                    value={year}
                >
                    {year}
                </Select.Option>
            ))}
        </Select>
    )
}

function useYears(datejs: Dayjs, offset: number) {
    const years = useMemo(() => {
        const year = datejs.year()
        const result = []
        for (let i = year - offset; i < year + offset; i += 1) {
            result.push(i)
        }
        return result
    }, [datejs.year()])
    return years
}