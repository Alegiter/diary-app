import { Container } from "typedi"
import { AppEntry, AppEntryDraft } from "@app/model/AppEntry"
import dayjs from "dayjs"
import { AppJson } from "@app/utils/json"
import { SearchController, SearchController$Token } from "./SearchController"
import { AppDataBase$Token } from "../db/AppDataBase"
import { AppEntryFilter } from "@app/model/AppEntryFilter"

describe("SearchController test", () => {
    const entry: AppEntryDraft = {
        date: dayjs("2023/8/25").startOf("D").toDate(),
        text: "test"
    }
    let controller: SearchController

    beforeAll(async () => {
        Container.set(IDBFactory, self.indexedDB)
        controller = Container.get(SearchController$Token)

        const db = Container.get(AppDataBase$Token)
        await db.add(entry)
    })

    it("should find by date", async () => {
        const { stringify, parse } = AppJson
        const filter: AppEntryFilter = {
            date: entry.date,
        }

        const reqPut = new Request("", {
            method: "POST",
            body: stringify(filter)
        })
        const [result,] = await controller.post(reqPut)
            .then(response => response.text())
            .then(text => parse<Array<AppEntry>>(text))

        expect(result.date).toEqual(entry.date)
    })
    it("should find by period", async () => {
        const { stringify, parse } = AppJson
        const filter: AppEntryFilter = {
            period: {
                from: dayjs(entry.date).subtract(1, "day").toDate(),
                to: dayjs(entry.date).add(1, "day").toDate()
            }
        }

        const reqPut = new Request("", {
            method: "POST",
            body: stringify(filter)
        })
        const [result,] = await controller.post(reqPut)
            .then(response => response.text())
            .then(text => parse<Array<AppEntry>>(text))

        expect(result.date).toEqual(entry.date)
    })
})