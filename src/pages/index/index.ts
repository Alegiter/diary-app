import "reflect-metadata"
import { rootRender } from "@app/utils/rootRender"
import { App } from "./App";

console.log("Index page with search input and calendar, and today entry is chosen");

rootRender(App)